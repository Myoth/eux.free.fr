#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'


import grequests
import sys
import time
import argparse
# ------------- gestion du ctrl-c en cas de blocage
from signal import signal, SIGINT
from sys import exit


def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(1)


signal(SIGINT, handler)
# ----------------------------------------------------


def analyze(liste, domaine, lot=100):
    """analyze les prenoms dans les sous-domaines
        par un traitement en parallele
    Parameters
    ----------
    liste :
        liste de prenoms ( ex: ['nico', 'jean']).
    domaine :
        domaine de recherche (ex: 'free.fr').

    Returns
    -------
        retourne la liste des prenom existants dans le sous-domaine
    """
    listePresents = []
    for i in range(0, len(liste), lot):
        async_list = []
        limiteBasse = i
        limiteHaute = i+lot if i+lot < len(liste) else len(liste)
        # print(len(liste), limiteBasse, limiteHaute)
        for ind, prenom in enumerate(liste[limiteBasse:limiteHaute]):
            prenom = prenom.strip()
            action_item = grequests.get(f'http://{prenom}.{domaine}', timeout=0.1)
            async_list.append(action_item)
        retour = grequests.map(async_list)
        for indice, j in enumerate(retour):
            if j is not None and 200 == j.status_code:
                print(j.status_code, liste[indice+i])
                listePresents.append(liste[indice])
        print(listePresents)
    return listePresents


def sauve(liste, domaine, fichier):
    """sauvegarde sous forme HTML

    Parameters
    ----------
    liste :
        liste des prenoms exisants
    domaine:
        domaine de recherche
    fichier :
        fichier de sauvegarde HTML
    """
    with open(fichier, 'w') as f:
        f.write('<html>\n<body>\n')
        for prenom in liste:
            f.write(f'<a href="http://{prenom.strip()}.{domaine.strip()}" target=_blank>{prenom.strip()}</a><br>\n')
        f.write('</body>\n</html>')


# sys.argv = ['', '-p', 'bases/prenoms']
# création du parse des arguments
parser = argparse.ArgumentParser(description="Scanner")
# déclaration et configuration des arguments
parser.add_argument('-dbg', '--debugmode', action="store_true", default=False, help="active le mode debug (limite le nbr de prenoms traites)")
parser.add_argument('-p', '--prenomfile', type=str, action="store", default="prenoms", help="fichier contenant 1 prenom par ligne")
parser.add_argument('-s', '--savefile', type=str, action="store", default="eux_async.html", help="fichier de sauvegarde html")
parser.add_argument('-d', '--domaine', type=str, action="store", default="free.fr", help="domaine de recherche")
parser.add_argument('-l', '--nbrparlot', type=int, action="store", default=100, help="nombre de traitement en 1 fois")
dargs = vars(parser.parse_args())
# print(dargs)


debut = time.time()
# lit le fichier
if not dargs['debugmode']:
    listePrenom = open(dargs['prenomfile'], 'r').readlines()
    ret = input(f'Vous etes sur de vouloir analyser tous les {len(listePrenom)} prenoms y/[N]!!!???')
    if ret.upper() != 'Y':
        exit(0)
else:
    listePrenom = open(dargs['prenomfile'], 'r').readlines()[:100]
# analyze et retourne la liste des prenoms qui match
res = analyze(listePrenom, dargs['domaine'], int(dargs['nbrparlot']))
# sauve les prenoms qui match
sauve(res, dargs['domaine'], dargs['savefile'])
fin = time.time()
print(f'temps de fonctionnement: {(fin-debut)} secondes')
