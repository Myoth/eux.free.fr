#!/bin/bash

if [ -z "$1" ] ; then
	echo "Utilisation : $0 <liste_noms>"
	exit 1
fi

liste_noms="$1"
sortie="eux2.html"
nb_noms=$(cat $liste_noms | wc -l )
nb_invalides=0
nb_valides=0
lettre=$(head -c 1 $liste_noms)
echo "On en est au $lettre..."

for prenom in $(cat $liste_noms) ; do
	ancienne_lettre=$lettre
	lettre=$(echo $prenom | head -c 1)
	if [ "$lettre" != "$ancienne_lettre" ]; then
		echo "On en est au $lettre..."
	fi

	# echo "Essai ${prenom}.pagesperso-orange.fr..."
	retour=$(curl -Iw "%{http_code}" ${prenom}.pages-perso-orange.fr 2>/dev/null | tail -n 1)
	# echo $retour
	if [ $retour -eq 200 ] ; then
		echo "Site valide trouvé : http://${prenom}.pages-perso-orange.fr"
		echo "<a href=http://$prenom.pages-perso-orange.fr target=_blank>$prenom</a>" >> $sortie
		echo "<br>" >> $sortie
		((nb_valides++))
	else
		((nb_invalides++))
	fi
done

echo "$nb_invalides" sites invalide\(s\) , "$nb_valides" site valide\(s\) \(sur "$nb_noms" essais\)
