# eux.free.fr

## Bases

*nat2018.csv* --> fichier de base de l'INSEE
 
*prenoms.txt* --> fichier formaté avec `tail -n+2 nat2018.csv | cut -d ";" -f2 | awk '{print tolower($0)}' | iconv -f utf8 -t ascii//TRANSLIT | sort | uniq >> prenoms`

*mots_fr* --> contruit à partir d'une liste trouvée [ici](http://infolingu.univ-mlv.fr/DonneesLinguistiques/Dictionnaires/telechargement.html). La deuxième liste (wml et utf-8) a été utilisée. 
Le processus de formattage utilisé étant un peu complexe, il ne sera pas détaillé ici, afin que si vous souhatiez le faire, vous le fassiez mieux. 

## Script

Utilisation : `./eux.sh <liste_prenoms>`

Le script avec la liste *prenoms.txt* prends un certain temps, le miux est de le laisser tourner tout seul.

La liste des sites trouvés est dans `eux.html`, formatée pour être intégrée à une page web.
