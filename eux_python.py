#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'


import requests
import sys
import time
# ------------- gestion du ctrl-c en cas de blocage
from signal import signal, SIGINT
from sys import exit


def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(1)


signal(SIGINT, handler)
# ----------------------------------------------------


# sys.argv = ['', 'bases/prenoms', 'free.fr', 'eux.html']
# sys.argv = ['', 'prenom_fonctionnel.txt', 'free.fr', 'eux.html']
if len(sys.argv) < 4:
    print(f"usage: {sys.argv[0]} fichierListePrenom domaine fichierSauvegardeHtml")
    print("-> fichierListePrenom    : fichier contenant 1 prenom par ligne")
    print("-> domaine               : domaine de recherche (exemple: 'free.fr')")
    print("-> fichierSauvegardeHtml : fichier ou sera sauvegarders les resultats sous forme html")
    sys.exit(1)


def analyze(liste, domaine):
    """analyze les prenoms dans les sous-domaines

    Parameters
    ----------
    liste :
        liste de prenoms ( ex: ['nico', 'jean']).
    domaine :
        domaine de recherche (ex: 'free.fr').

    Returns
    -------
        retourne la liste des prenom existants dans le sous-domaine
    """
    total = len(liste)
    indice = 0
    while indice != total:
        prenom = liste[indice].strip()
        # print(f'analyze {indice+1}/{total}')
        try:
            if requests.get(f'http://{prenom}.{domaine}', timeout=0.1).status_code != 200:
                liste.pop(indice)
                total -= 1
            else:
                indice += 1
        except Exception:   # le timeout a ete atteint on estime que le site down
            liste.pop(indice)
            total -= 1
    return liste


def sauve(liste, domaine, fichier):
    """sauvegarde sous forme HTML

    Parameters
    ----------
    liste :
        liste des prenoms exisants
    domaine:
        domaine de recherche
    fichier :
        fichier de sauvegarde HTML
    """
    with open(fichier, 'w') as f:
        f.write('<!DOCTYPE html>\n<html>\n<body>\n')
        for prenom in liste:
            f.write(f'<a href="http://{prenom.strip()}.{domaine.strip()}" target=_blank>{prenom.strip()}</a><br>\n')
        f.write('</body>\n</html>\n')


debut = time.time()
listePrenom = open(sys.argv[1], 'r').readlines()[:100]
res = analyze(listePrenom, sys.argv[2])
sauve(res, sys.argv[2], sys.argv[3])
fin = time.time()
print(f'temps de fonctionnement: {(fin-debut)} secondes')
